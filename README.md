# Commerce SQL Sanitize

This module extends the `drush sql:sanitize` command to do the following:

* Sanitize order email and IP addresses
* Sanitize addresses (converting most values to `[Sanitized]`)
* Delete all commerce logs (e.g., from the order activity stream)
* Delete all carts, though not all _Draft_ orders
* Delete all tax numbers from customer profiles
* Delete all payment methods

Command line options let you disable any of these operations.

## Usage

To see a list of all options this module respects, use:

```
drush sql:sanitize --help
```

These options are typically _disabling_ default behavior of this module, though in some cases you can also direct how sanitization should target certain elements. You can pass as few or as many options to the command as you want. For example, with this command your logs and payment methods will not be deleted:

```
drush sql:sanitize --sanitize-commerce-log=no --sanitize-commerce-payment-method=no
```

Note: you can also use `0` in place of `no` to disable these various features.

### Sanitizing orders

Order email and IP addresses will be sanitized by default.

Disable this with the following option:

```
drush sql:sanitize --sanitize-orders=no
```

### Santitizing addresses

All address fields on profiles are sanitized by default. To preserve the apperance of content, their text values will be replaced with `[Sanitized]` and administrative area, postal code, and country defaulted to a generic location.

Disable this with the following option:

```
drush sql:sanitize --sanitize-address-field=no
```

You can also restrict sanitization to named fields on the profiles, ignoring all other fields, by passing the following option:

```
# Sanitize only a single field
drush sql:sanitize --sanitize-address-fields=address

# Sanitize multiple named fields
drush sql:sanitize --sanitize-address-fields=address,field_additional_address
```

### Delete all Commerce logs

All Commerce Log entities are deleted (if the Commerce Log module is enabled).

Disable this with the following option:

```
drush sql:sanitize --sanitize-commerce-log=no
```

### Delete all carts

All carts, though not all orders in the _Draft_ state, will be deleted. This is to avoid the situation where in progress orders have data that conflicts with non-production settings.

Disable this with the following option:

```
drush sql:sanitize --delete-carts=no
```

### Delete all tax numbers

All field and revision values for the tax number field Commerce adds to profile types will be deleted.

Disable this with the following option:

```
drush sql:sanitize --delete-tax-numbers=no
```

### Delete all payment methods
All payment methods will be deleted, as they are tied to specific sets of API credentials and cannot be reused between environments. (e.g., a credit card saved in a live Stripe account cannot be used in the same account's developer mode or a sandbox.)

Disable this with the following option:

```
drush sql:sanitize --sanitize-commerce-payment-method=no
```

## Feature requests

Request new features or ideas for improvement in the module's issue queue:

https://www.drupal.org/project/issues/commerce_sql_sanitize
