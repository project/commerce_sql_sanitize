<?php

namespace Drupal\commerce_sql_sanitize\Commands;

use Drush\Drupal\Commands\sql\SanitizePluginInterface;

/**
 * Provides a Drush sql-sanitize plugin for payment methods.
 */
class TruncatePaymentMethodsCommands extends TruncateEntityTablesCommands implements SanitizePluginInterface {

  /**
   * {@inheritdoc}
   */
  protected function getEntityTypeIds() {
    return ['commerce_payment_method'];
  }

}
