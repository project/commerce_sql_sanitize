<?php

namespace Drupal\commerce_sql_sanitize\Commands;

use Consolidation\AnnotatedCommand\CommandData;
use Drupal\Core\Database\Connection;
use Drush\Commands\DrushCommands;
use Drush\Drupal\Commands\sql\SanitizePluginInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;

/**
 * Provides a drush sql:sanitize plugin for commerce_tax_number field.
 */
class SanitizeTaxNumber extends DrushCommands implements SanitizePluginInterface {

  /**
   * The database connection.
   *
   * @var \Drupal\Core\Database\Connection
   */
  protected $database;

  /**
   * Creates a new SanitizeTaxNumber object.
   *
   * @param \Drupal\Core\Database\Connection $database
   *   The database connection.
   */
  public function __construct(Connection $database) {
    parent::__construct();
    $this->database = $database;
  }

  /**
   * {@inheritdoc}
   *
   * Truncates profile__tax_number, profile_revision__tax_number tables.
   *
   * @hook post-command sql-sanitize
   */
  public function sanitize($result, CommandData $commandData) {
    $options = $commandData->options();

    if (!$this->isEnabled($options['sanitize-tax-number'] ?? 1)) {
      return;
    }

    $this->database->truncate('profile__tax_number')->execute();
    if ($this->database->schema()->tableExists('profile_revision__tax_number')) {
      $this->database->truncate('profile_revision__tax_number')->execute();
    }
  }

  /**
   * {@inheritdoc}
   *
   * @hook on-event sql-sanitize-confirms
   */
  public function messages(&$messages, InputInterface $input) {
    $options = $input->getOptions();
    if (!$this->isEnabled($options['delete-tax-numbers'] ?? 1)) {
      return;
    }
    $messages[] = dt('Delete all tax numbers from customer profiles.');
  }

  /**
   * Provides extra options for sql:sanitize.
   *
   * @hook option sql-sanitize
   */
  public function options(Command $command) {
    if (!$this->database->schema()->tableExists('profile__tax_number')) {
      return;
    }
    $command->addOption(
      'delete-tax-numbers',
      NULL,
      InputOption::VALUE_OPTIONAL,
      dt("By default, all tax numbers on customer profiles will be deleted. Specify <info>no</info> to disable that.")
    );
  }

  /**
   * Test an option value to see if it is enabled.
   *
   * @param string $value
   *   The option value to test.
   *
   * @return bool
   *   TRUE if the value represents a positive value (anything other than 'no'
   *   or '0').
   */
  protected function isEnabled($value) {
    if (!$this->database->schema()->tableExists('profile__tax_number')) {
      return FALSE;
    }
    return $value != 'no' && $value != '0';
  }

}
