<?php

namespace Drupal\commerce_sql_sanitize\Commands;

/**
 * Provides a Drush sql-sanitize plugin for Commerce logs.
 */
class TruncateCommerceLogCommands extends TruncateEntityTablesCommands {

  /**
   * {@inheritdoc}
   */
  protected function getEntityTypeIds() {
    return ['commerce_log'];
  }

}
