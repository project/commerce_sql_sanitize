<?php

namespace Drupal\commerce_sql_sanitize\Commands;

use Consolidation\AnnotatedCommand\CommandData;
use Drupal\Core\Database\Connection;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drush\Commands\DrushCommands;
use Drush\Drupal\Commands\sql\SanitizePluginInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;

/**
 * Provides a Drush sql sanitization plugin for orders.
 *
 * It can delete carts and sanitize order email and IP addresses.
 */
class SanitizeOrderCommands extends DrushCommands implements SanitizePluginInterface {

  /**
   * The database connection.
   *
   * @var \Drupal\Core\Database\Connection
   */
  protected $database;

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Creates a new SanitizeOrderCommands object.
   *
   * @param \Drupal\Core\Database\Connection $database
   *   The database connection.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   */
  public function __construct(Connection $database, EntityTypeManagerInterface $entity_type_manager) {
    parent::__construct();
    $this->database = $database;
    $this->entityTypeManager = $entity_type_manager;
  }

  /**
   * {@inheritdoc}
   *
   * Deletes carts and sanitizes order email and IP addresses.
   *
   * @hook post-command sql-sanitize
   */
  public function sanitize($result, CommandData $commandData) {
    $options = $commandData->options();

    if ($this->isEnabled($options['delete-carts'] ?? 1)) {
      $order_storage = $this->entityTypeManager->getStorage('commerce_order');
      $cart_order_ids = $order_storage->getQuery()
        ->condition('cart', TRUE)
        ->accessCheck(FALSE)
        ->execute();

      if (!empty($cart_order_ids)) {
        foreach (array_chunk($cart_order_ids, 25) as $order_ids) {
          $orders = $order_storage->loadMultiple($order_ids);
          $order_storage->delete($orders);
        }

        $this->logger()->success(dt('All @count cart orders deleted.', ['@count' => count($cart_order_ids)]));
      }
    }

    if ($this->isEnabled($options['sanitize-orders'] ?? 1)) {
      $this->database->update('commerce_order')
        ->isNotNull('mail')
        ->fields(['mail' => 'sanitized@local.test'])
        ->execute();
      $this->database->update('commerce_order')
        ->isNotNull('ip_address')
        ->fields(['ip_address' => '127.0.0.1'])
        ->execute();

      $this->logger()->success(dt('Order email and IP addresses sanitized.'));
    }

    $this->entityTypeManager->getStorage('commerce_order')->resetCache();
  }

  /**
   * Provides extra options for sql:sanitize.
   *
   * @hook option sql-sanitize
   */
  public function options(Command $command) {
    if (!$this->entityTypeManager->getDefinition('commerce_order', FALSE)) {
      return;
    }
    $command->addOption(
      'sanitize-orders',
      NULL,
      InputOption::VALUE_OPTIONAL,
      dt('By default, order email and IP addresses are sanitized. Specify <info>no</info> to disable that.')
    );
    $command->addOption(
      'delete-carts',
      NULL,
      InputOption::VALUE_OPTIONAL,
      dt('By default, all carts and their related entities are deleted. Specify <info>no</info> to disable that.')
    );
  }

  /**
   * {@inheritdoc}
   *
   * @hook on-event sql-sanitize-confirms
   */
  public function messages(&$messages, InputInterface $input) {
    $options = $input->getOptions();
    if ($this->isEnabled($options['sanitize-orders'] ?? 1)) {
      $messages[] = dt('Sanitize order email and IP addresses.');
    }
    if ($this->isEnabled($options['delete-carts'] ?? 1)) {
      $messages[] = dt('Delete all carts and related entities.');
    }
  }

  /**
   * Test an option value to see if it is enabled.
   *
   * @param string $value
   *   The option value to test.
   *
   * @return bool
   *   TRUE if the value represents a positive value (anything other than 'no'
   *   or '0').
   */
  protected function isEnabled($value) {
    if (!$this->entityTypeManager->getDefinition('commerce_order', FALSE)) {
      return FALSE;
    }
    return $value != 'no' && $value != '0';
  }

}
