<?php

namespace Drupal\commerce_sql_sanitize\Commands;

use Consolidation\AnnotatedCommand\CommandData;
use Drupal\Core\Database\Connection;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Entity\Sql\SqlEntityStorageInterface;
use Drush\Commands\DrushCommands;
use Drush\Drupal\Commands\sql\SanitizePluginInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;

/**
 * Provides a base class for drush sanitize plugins that truncate entity tables.
 */
abstract class TruncateEntityTablesCommands extends DrushCommands implements SanitizePluginInterface {

  /**
   * The database connection.
   *
   * @var \Drupal\Core\Database\Connection
   */
  protected $database;

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Creates a new TruncateEntityTablesCommands object.
   *
   * @param \Drupal\Core\Database\Connection $database
   *   The database connection.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   */
  public function __construct(Connection $database, EntityTypeManagerInterface $entity_type_manager) {
    parent::__construct();
    $this->database = $database;
    $this->entityTypeManager = $entity_type_manager;
  }

  /**
   * {@inheritdoc}
   *
   * Truncates all configured entity tables.
   *
   * @hook post-command sql-sanitize
   */
  public function sanitize($result, CommandData $commandData) {
    $options = $commandData->options();
    foreach ($this->getFilteredEntityTypeIds() as $entity_type_id) {
      if ($this->isEnabled($options[$this->getOptionName($entity_type_id)])) {
        $this->truncateEntityTables($entity_type_id);
      }
    }
  }

  /**
   * Truncates all the entity tables for the entity type.
   *
   * @param string $entity_type_id
   *   The entity type ID whose tables will be truncated.
   */
  protected function truncateEntityTables($entity_type_id) {
    $storage = $this->entityTypeManager->getStorage($entity_type_id);
    $entity_type = $this->entityTypeManager->getDefinition($entity_type_id);
    if (!$storage instanceof SqlEntityStorageInterface) {
      $context = [
        '@entity_type' => $entity_type->getSingularLabel(),
        '@interface' => SqlEntityStorageInterface::class,
      ];
      $this->logger()
        ->warning(dt('Unable to truncate @entity_type entity tables: the storage handler does not implement @interface.', $context));
      return;
    }
    $mapping = $storage->getTableMapping();
    foreach ($mapping->getTableNames() as $table_name) {
      $this->database->truncate($table_name)->execute();
    }
    $storage->resetCache();
    $context = ['@entity_type' => $entity_type->getLabel()];
    $this->logger()
      ->success(dt('@entity_type entity tables truncated.', $context));
  }

  /**
   * Provides options for the command.
   *
   * All configured entity types have their tables sanitized by default, but it
   * can be disabled per entity type.
   *
   * @hook option sql-sanitize
   */
  public function options(Command $command) {
    foreach ($this->getFilteredEntityTypeIds() as $entity_type_id) {
      $entity_type = $this->entityTypeManager->getDefinition($entity_type_id);
      $context = ['@entity_type' => $entity_type->getSingularLabel()];
      $command->addOption(
        $this->getOptionName($entity_type_id),
        NULL,
        InputOption::VALUE_OPTIONAL,
        dt('By default, @entity_type entity tables are truncated. Specify <info>no</info> to disable that.', $context)
      );
    }
  }

  /**
   * {@inheritdoc}
   *
   * @hook on-event sql-sanitize-confirms
   */
  public function messages(&$messages, InputInterface $input) {
    foreach ($this->getFilteredEntityTypeIds() as $entity_type_id) {
      if ($this->isEnabled($input->getOption($this->getOptionName($entity_type_id)))) {
        $entity_type = $this->entityTypeManager->getDefinition($entity_type_id);
        $context = ['@entity_type' => $entity_type->getSingularLabel()];
        $messages[] = dt('Truncate @entity_type entity tables.', $context);
      }
    }
  }

  /**
   * Test an option value to see if it is enabled.
   *
   * @param string $value
   *   The value.
   *
   * @return bool
   *   TRUE if the value represents a positive value (anything other than 'no'
   *   or '0').
   */
  protected function isEnabled($value) {
    return $value != 'no' && $value != '0';
  }

  /**
   * Returns the option name to disable truncating entity tables.
   *
   * @param string $entity_type_id
   *   The entity type ID the option name is for.
   *
   * @return string
   *   The option name used to disable truncating entity tables.
   */
  protected function getOptionName(string $entity_type_id) {
    return 'sanitize-' . str_replace('_', '-', $entity_type_id);
  }

  /**
   * Returns an array of the filtered entity type IDs.
   */
  protected function getFilteredEntityTypeIds() {
    return array_filter($this->getEntityTypeIds(), function (string $entity_type_id) {
      return $this->entityTypeManager->getDefinition($entity_type_id, FALSE);
    });
  }

  /**
   * Returns an array of entity type IDs whose tables will be truncated.
   */
  abstract protected function getEntityTypeIds();

}
